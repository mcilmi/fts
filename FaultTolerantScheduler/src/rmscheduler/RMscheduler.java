package rmscheduler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class RMscheduler {
    private int time;
    QueueManager QM;


    public RMscheduler(QueueManager queueManager){
        this.QM = queueManager;
    }

    public void executeTasks(){
        List<Task> queue = new ArrayList<>();
        queue = copyTasks(QM.queue);
        Timer timer;
        for(Task task : queue){
            timer = new Timer();
            try {
                Thread.sleep((long) task.getCompTime()*1000);
                System.out.println("Successfully executed task: " + task.getName());
                QM.queue.remove(task);
            } catch (InterruptedException e) {
                System.out.println("Threw Exception!");
                task.setCompTime(task.getCompTime() - (timer.start - timer.elapsedTime()));
            }

        }
    }
    
    /**
     * Do RM analysis and determine if there is a feasible schedule or not.
     *
     * @return true if feasible else false
     */
    public boolean isFeasible() {        
        int numberOfTasks;
        numberOfTasks = QM.queue.size();
        double util;
        util = numberOfTasks * (Math.pow((double) 2, (double) 1 / numberOfTasks) - 1);
        int x = 0;
        double sum = 0;

        for (int i = 0; i < numberOfTasks; i++) {
            sum += (double) QM.queue.get(i).getCompTime() / QM.queue.get(i).getPeriod();
        }
        System.out.println("Util limit:" + new BigDecimal(util).setScale(2, RoundingMode.HALF_UP).doubleValue());
        System.out.println("Util Sum: " + new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).doubleValue());

        return sum <= util;
    }

    public void schedule(){
        Task first;
        for(int cnt=0; cnt<QM.queue.size()-1; cnt++){
            first = QM.queue.get(cnt);
            if(first.getPeriod()>QM.queue.get(cnt+1).getPeriod()){
                moveTaskFirst(cnt);
            }

        }
    }

    public void moveTaskFirst(int taskid){
        Task task = QM.queue.get(taskid);
        QM.queue.add(0, task);
        int i = QM.queue.lastIndexOf(task);
        QM.queue.remove(i);
    }

    public List<Task> copyTasks(List<Task> queue){
        List<Task> q = new ArrayList<>();

        for(Task task: queue){
            q.add(task);
        }
        return q;
    }

}
