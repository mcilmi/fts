package rmscheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QueueManager {

    public  List<Task> queue = null;
    Timer timer = new Timer();
    private String fileName;
    private int maxTime = 0;

    public QueueManager() {
        fileName = "FaultTolerantScheduler/tasks.txt";
        queue = new ArrayList<>();
        try {
            populateQueue();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public QueueManager(String filename) throws IOException {
        queue = new ArrayList<>();
        fileName = filename;
        populateQueue();
    }

    private void add(Task task) {
        //task.setPeriod(task.getPeriod() + (int) timer.elapsedTime());
        queue.add(task);
    }

    public Task remove(int index) {
        return queue.remove(index);
    }

    public void printQueue() {
        for (Task task : queue) {
            System.out.println("Name: " + task.getName() + ", Computation time: "
                    + task.getCompTime()
                    + ", Period: " + task.getPeriod() + ", Priority: " + task.getPriority());
        }
    }

    public void setFileName(String filename) {
        fileName = filename;
    }

    private String getFileName() {
        return fileName;
    }

    /**
     * Parse the file that contains the task list and populate the queue.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void populateQueue() throws FileNotFoundException, IOException {
        FileReader file = new FileReader(getFileName());
        BufferedReader bufferedReader = new BufferedReader(file);
        String line;
        String[] parts;
        Task task;
        //Split the line and parse the parameters.
        while ((line = bufferedReader.readLine()) != null) {
            parts = line.split(",");
            task = new Task(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
            int priority;
            priority = ((int)Math.floor(((double)1/task.getPeriod())*100));
            task.setPriority(priority);
            queue.add(task);
            setMaxTime(Integer.parseInt(parts[1]));
        }
        file.close();
    }   
    
    private void setMaxTime(int time){
        maxTime += time;
    }
    public int getMaxTime(){
        return maxTime;
    }
    public int size() {
        return queue.size();
    }


}
