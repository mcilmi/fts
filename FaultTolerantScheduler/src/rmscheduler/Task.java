package rmscheduler;

public class Task {

    private String name;
    private int taskID;
    private double compTime;
    private int period;
    private int priority = -1;
    public Task(String name, double execTime, int period) {
        this.name = name;
        this.compTime = execTime;
        this.period = period;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public double decrementCompTime(){
        return --this.compTime;
    }
    public double getCompTime() {
        return this.compTime;
    }
    public void setCompTime(double time){   this.compTime = time;    }

    public int getPeriod() {
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
    
    public void setPriority(int priority){
        this.priority = priority;
    }
    
    public int getPriority(){
        return this.priority;
    }

    @Override
    public String toString() {
        return this.getName() + " Computation time: " + this.getCompTime() + " Deadline: " + this.getPeriod();
    }

}
