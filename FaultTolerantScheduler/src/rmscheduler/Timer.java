package rmscheduler;

public class Timer {

    public final long start;

    public Timer() {
        this.start = System.currentTimeMillis();
    }

    /**
     * Calculate the elapsed time since start
     * @return the elapsed time(now - start)
     */
    public double elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start) / 1000.0;
    }

}
