package rmscheduler;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class FaultTolerantScheduler{
    QueueManager queue;
    RMscheduler scheduler;

    public FaultTolerantScheduler(){
        this.queue = new QueueManager();
        this.scheduler = new RMscheduler(queue);

        if (scheduler.isFeasible()) {
            System.out.println("The task set is schedulable!");
            scheduler.schedule();

        } else {
            System.out.println("The task set is not schedulable!");
        }

        queue.printQueue();
        System.out.println("--------------------------------------------\n");

        scheduler.executeTasks();
    }

    public static void main(String[] args) throws IOException {

        FaultTolerantScheduler fts = new FaultTolerantScheduler();
        Thread backup = new Thread(new Backup());
        System.out.println("\n\nBACKUP THREAD STARTS!");
        backup.start();

    }
}
